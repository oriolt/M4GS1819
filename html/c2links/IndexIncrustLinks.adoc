= Incrustant objectes en un document HTML
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:

- link:e1-incrustem-links.html[Incrustar enllaços absoluts i relatius]
- link:e1-crear-un-titol.html[Definir un títol]
- link:e2-creantcapacaleres.html[Definir capçaleres]
- link:e3-articles-vs-sections.html[Definició dels articles i diferència amb les seccions]
- link:e4-agrupant-hgroup.html[Quina és la funció de l'hgroup?]
- link:e5-elements-capçalera.html[Definint les capçaleres dels elements que integren un document HTML]
- link:e6-Marques-de-navegacio.html[Les marques de navegació (àncores)]
- link:e7-definir-un-seccio.html[Definir una secció]
- link:e8-Marques-de-navegacio.html[El bloc aside]
- link:e9-crear-un-footer.html[El bloc footer]

