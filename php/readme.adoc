# Llenguatge PHP: Índex d'exemples
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:

## Sintaxis bàsica
- link:exemples/_a_variables_internes/exemple1.php[Variables i echo]
- link:exemples/_a_variables_internes/exemple2.php[Cometes simples i dobles]
- link:exemples/_b_estructures/exemple3.php[Estructura if]
- link:exemples/_b_estructures/exemple4.php[Estructura if - else]
- link:exemples/_b_estructures/exemple5.php[Estructura elseif]
- link:exemples/_b_estructures/exemple6.php[Estructura switch - case - break]
- link:exemples/_b_estructures/exemple7.php[Estructura while]
- link:exemples/_b_estructures/exemple8.php[Estructura do - while]
- link:exemples/_b_estructures/exemple9.php[Estructura for]
- link:exemples/_b_estructures/exemple10.php[Estructura for amb taules]
- link:exemples/_b_estructures/exemple11.php[Arrays]
- link:exemples/_b_estructures/exemple12pre.php[Introducció a les funcions]
- link:exemples/_b_estructures/exemple12.php[Funcions]
- link:exemples/_b_estructures/exemple13.php[Array indexat i Array associatiu]

## Procediments i funcions
- link:exemples/_c_procediments_i_funcions/exemple13bis.php[Funcions (II)]
- link:exemples/_c_procediments_i_funcions/exemple14.php[Funcions (III)]
- link:exemples/_c_procediments_i_funcions/exemple15.php[Pas de paràmetres per valor (entrada)]
- link:exemples/_c_procediments_i_funcions/exemple16.php[Pas de paràmetres (sortida)]
- link:exemples/_c_procediments_i_funcions/pas_de_parametres/get/index.html[Pas de paràmetres per GET]
- link:exemples/_c_procediments_i_funcions/pas_de_parametres/get/index2.html[Pas de paràmetres per GET (II)]
- link:exemples/_c_procediments_i_funcions/pas_de_parametres/post/index.html[Pas de paràmetres per POST]
- link:exemples/_c_procediments_i_funcions/pas_de_parametres/post/index2.html[Pas de paràmetres per POST (II)]
- link:exemples/_c_procediments_i_funcions/pas_de_parametres/isset/index.html[Comprovació de variables amb isset]

## Sessions
- link:exemples/_d_sessions/index.php[Login: Usuari i password]

## Excepcions
- link:exemples/_e_excepcions/index.php[Gestió d'excepcions]

## Bases de dades
- link:exemples/_d_sessions/exemple21.php[Sessions: Usuari i password (II)]

= Activitats:
- link:activitats/_A_sintaxi_basica.adoc[Sintaxi bàsica]
- link:activitats/_B_Sessions.adoc[Sessions]
- link:activitats/_C_excepcions.adoc[Excepcions]
- link:activitats/_D_Select.adoc[Select]
- link:activitats/_D_insert.adoc[Insert]