<?php
function checkEmail($email) {
  $atPos=strpos($email, "@");
  if ($atPos===false || $atPos<1) {
    throw new Exception("Correu electrònic invàlid.");
  }
  $dotPos=strpos($email, ".");
  if ($dotPos===false || strlen($email)-$dotPos-1<2 || strlen($email)-$dotPos-1>3) {
    throw new Exception("Correu electrònic invàlid.");
  }
}

function checkPhone($phone) {
  if (strlen($phone)<9 || strlen($phone)>13) {
    throw new Exception("Número de telèfon erroni.");
  }
  for ($pos=0; $pos<strlen($phone); $pos++) {
    if (($pos!=0 || $phone[$pos]!='+') && strpos("0123456789", $phone[$pos])===false) {
      throw new Exception("Número de telèfon erroni.");
    }
  }
}

function parseDate($date) {
  $parsedDate = date_parse($date);
  if ($parsedDate['error_count']>0) {
    throw new Exception("La data no té un format vàlid.");
  }
  $strDate = "{$parsedDate['year']}-{$parsedDate['month']}-{$parsedDate['day']}";
  return $strDate;
}

function checkStayDates($checkInDate, $checkOutDate) {
  if ($checkInDate>=$checkOutDate) {
    throw new Exception("La data de sortida ha de ser posterior a la d'entrada.");
  }
  $today = date('Y-m-d');
  if ($today>=$checkInDate) {
    throw new Exception("La data d'entrada ha de ser posterior a avui.");
  }
}

session_start();
try {
  if (!isset($_POST['firstname']) ||
      !isset($_POST['lastname']) ||
      !isset($_POST['email']) ||
      !isset($_POST['nationality']) ||
      !isset($_POST['phone']) ||
      !isset($_POST['checkin']) ||
      !isset($_POST['checkout']) ||
      !isset($_POST['nhosts'])
  ) {
    throw new Exception("Falten paràmetres.");
  }
  $firstname = trim($_POST['firstname']);
  $lastname = trim($_POST['lastname']);
  $email = trim($_POST['email']);
  $nationality = trim($_POST['nationality']);
  $phone = trim($_POST['phone']);
  $checkIn = trim($_POST['checkin']);
  $checkOut = trim($_POST['checkout']);
  $nHosts = trim($_POST['nhosts']);

  if (strlen($firstname)<2 || strlen($lastname)<2) {
    throw new Exception("El nom i cognom han de tenir dos caràcters mínim.");
  }
  checkEmail($email);
  if (strlen($nationality)<2) {
    throw new Exception("Cal especificar la nacionalitat.");
  }
  if (strlen($phone)>0) {
    checkPhone($phone);
  }
  $checkInDate = parseDate($checkIn);
  $checkOutDate = parseDate($checkOut);
  checkStayDates($checkInDate, $checkOutDate);
  $nHosts = intval($nHosts);
  if ($nHosts<1 || $nHosts>5) {
    throw new Exception("El nombre d'hostes ha d'estar entre 1 i 5.");
  }
} catch (Exception $e) {
  $_SESSION['error'] = $e->getMessage();
  header('Location: index.php');
  exit();
}
?>

<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Excepcions</title>
  </head>
  <body>
    <main role="main" class="container">
      <h1 class="mt-5">Exercici 2</h1>
      <div>
        <p>Les dades de la reserva són:</p>
        <ul>
          <li>Nom del client: <?php echo "$firstname $lastname"; ?></li>
          <li>Correu electrònic: <?php echo "$email"; ?></li>
          <li>Telèfon: <?php echo "$phone"; ?></li>
          <li>Entrada: <?php echo "$checkInDate"; ?></li>
          <li>Sortida: <?php echo "$checkOutDate"; ?></li>
          <li>Quantitat d'hostes: <?php echo "$nHosts"; ?></li>
        </ul>
        <p><a href="index.php">Torna al formulari...</a></p>
      </div>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
