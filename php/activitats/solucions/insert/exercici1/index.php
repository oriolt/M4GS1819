<?php
require_once 'Connection.php';

session_start();

function get_room_types() {
  $conn = connect();
  $statement = $conn->prepare("SELECT Id, Name FROM RoomTypes ORDER BY Id");
  $statement->execute();
  $roomTypes = $statement->fetchAll();
  return $roomTypes;
}

function show_room_types($roomTypes) {
  if (sizeof($roomTypes)==0) {
    echo "<p>No hi ha cap tipus d'habitació.</p>\n";
  } else {?>
    <div class="form-group">
    <label for="roomType">Tipus d'habitació</label>
    <select class="form-control" id="roomType" name="roomTypeId">
      <?php
      foreach ($roomTypes as $roomType) {
        echo "<option value='{$roomType['Id']}'>{$roomType['Name']}</option>\n";
      }
      ?>
    </select>
  </div>
  <?php
  }
}

function show_messages() {
  if (isset($_SESSION['error'])) {
    echo "<div class='alert alert-danger' role='alert'>{$_SESSION['error']}</div>";
    unset($_SESSION['error']);
  }
  if (isset($_SESSION['success'])) {
    echo "<div class='alert alert-success' role='alert'>{$_SESSION['success']}</div>";
    unset($_SESSION['success']);
  }
}
?>
<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Sentències INSERT</title>
  </head>
  <body>
    <?php show_messages(); ?>
    <main role="main" class="container">
      <h1 class="mt-5">Exercici 1</h1>
      <form action="ex1.php" method="post">
        <div class="form-group">
          <label for="roomNumber">Número d'habitació</label>
          <input type="text" class="form-control" name="roomNumber" id="roomNumber" placeholder="Escriu el número de l'habitació">
        </div>
        <?php
        try {
          show_room_types(get_room_types());
        } catch (Exception $e) {
          $error = $e->getMessage();
          echo "<div class='alert alert-danger' role='alert'>No s'ha pogut recuperar la llista de tipus d'habitació: $error</div>";
        }
        ?>
        <button type="submit" class="btn btn-primary">Insereix</button>
      </form>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
