<?php
require_once 'Connection.php';

session_start();

function checkRoom($conn, $roomNumber) {
  $roomExists = false;
  $st = $conn->prepare("SELECT RoomNumber FROM Rooms WHERE RoomNumber=:roomNumber");
  $st->bindParam(':roomNumber', $roomNumber);
  $st->execute();
  $result = $st->fetchAll();
  if (sizeof($result)>0) {
    $roomExists = true;
  }
  return $roomExists;
}

function insertRoom($conn, $roomNumber, $roomTypeId) {
  $statement = $conn->prepare(
    "INSERT INTO Rooms(RoomNumber, RoomTypeId)
    VALUES (:roomNumber, :roomTypeId)");
  $statement->bindParam(':roomNumber', $roomNumber);
  $statement->bindParam(':roomTypeId', $roomTypeId);
  $statement->execute();
}

try {
  if (!isset($_POST['roomNumber']) ||
      !isset($_POST['roomTypeId'])) {
    throw new Exception("Falten paràmetres.");
  }
  $roomNumber = trim($_POST['roomNumber']);
  $roomTypeId = trim($_POST['roomTypeId']);
  $conn = connect();
  if (checkRoom($conn, $roomNumber)) {
    throw new Exception("L'habitació $roomNumber ja existeix.");
  }
  insertRoom($conn, $roomNumber, $roomTypeId);
  $_SESSION['success'] = 'Habitació inserida correctament.';
  header('Location: index.php');
  exit();
} catch (Exception $e) {
  $_SESSION['error'] = $e->getMessage();
  header('Location: index.php');
  exit();
}

?>
