<?php
require_once 'Connection.php';

function show_errors() {
  if (isset($_SESSION['error'])) {
    echo "<div class='alert alert-danger' role='alert'>{$_SESSION['error']}</div>";
    unset($_SESSION['error']);
  }
}

function get_facilities() {
  $conn = connect();
  $statement = $conn->prepare("SELECT Id, Name FROM Facilities ORDER BY Id");
  $statement->execute();
  $facilities = $statement->fetchAll();
  return $facilities;
}

function show_facilities($facilities) {
  if (sizeof($facilities)==0) {
    echo "<p>No hi ha cap característica.</p>\n";
  }
  echo "<form class='form-inline' action='insert.php' method='post'>\n";
  echo "<table class='table table-striped'>\n<tr><th>Id</th><th>Nom</th><th>Acció</th></tr>\n";
  foreach ($facilities as $facility) {
    echo "<tr><td>{$facility['Id']}</td><td>{$facility['Name']}</td><td><a href='delete.php?id={$facility['Id']}'>Esborra</a></td></tr>\n";
  }
  echo "<tr><td></td><td>\n";
  echo "<label class='sr-only' for='name'>Name</label>";
  echo "<input type='text' class='form-control' name='name' id='name' placeholder='Nom'>";
  echo "</td>\n";
  echo "<td><button type='submit' class='btn btn-primary'>Insereix</button></td>";
  echo "</tr>\n";
  echo "</table>\n";
  echo "</form>\n";
}

session_start();
?>
<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Sentències INSERT i DELETE</title>
  </head>
  <body>
    <?php show_errors(); ?>
    <main role="main" class="container">
      <h1 class="mt-5">Gestiona característiques</h1>
      <?php
      try {
        show_facilities(get_facilities());
      } catch (Exception $e) {
        $error = $e->getMessage();
        echo "<div class='alert alert-danger' role='alert'>No s'ha pogut recuperar la llista de característiques: $error</div>";
      }
      ?>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
