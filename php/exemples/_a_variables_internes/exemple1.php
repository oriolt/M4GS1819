<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
   <title>Exemple (1) de variables internes de php</title>
</head>
<body>
   <main>
   <p>Concatenem dues cadenes de text amb l’operador punt (.) i imprimim el resultat.</p>
        <?php
            $ini = "Bon giorno ";
            $fi = " a tutti";
            $tot = $ini.$fi;
            echo $tot;
            echo "<h2>I ara, el mateix però afegint-hi etiquetes d'HTML i dobles cometes</h2>";
            echo "<h3>".$ini." ".$fi."</h3>";
            echo "<h2>I si afegim etiquetes d'HTML i cometes simples?</h2>";  
             echo '<h3>$ini.$fi</h3>';
          
            echo "</br>";
            echo "<h2>Ara farem el mateix però amb la suma de 2 valors. Definim dues variables que es sumin i que s’emmagatzemin dos números diferents.</h2>";
            $num1=1;
            $num2=2;
            echo "<h3>".$num1." ".$num2."</h3>";
            echo "<h3>$num1 $num2</h3>";
            echo '<h3>$num1 $num2</h3>';
            $suma =$num1+$num2;
            echo "$num1+$num2";
            echo "</br>";
            echo "Suma = ".$suma."<br>";
        ?>  
        </main>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>