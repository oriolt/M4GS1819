<?php
function show_indexed_array($array) {
  echo "Quantitat d'elements: ".count($array)."<br>";
  for ($i=0; $i<count($array); $i++) {
    echo "Índex=$i, valor={$array[$i]}<br>";
  }
}

function show_associative_array($array) {
  echo "Quantitat d'elements: ".count($array)."<br>";
  foreach($array as $key => $value) {
    echo "Clau=$key, valor=$value<br>";
  }
}
?>

<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Arrays</title>
  </head>
  <body>
    <main>
         <h1>Exemples d'array</h1>
      <?php
      echo "<h2>Array buit</h2>";
      $array = array();
      show_indexed_array($array);
      echo "<h2>Array amb elements</h2>";
      $array = array(3, 6, 9, 1);
      show_indexed_array($array);
      echo "<h2>Array associatiu</h2>";
      $array = array('a'=>3, 'b'=>6, 'c'=>9, 'd'=>1);
      show_associative_array($array);
      echo "<h2>Afegir elements a un array indexat</h2>";
      $array = array(3, 6, 9, 1);
      array_push($array, 2);
      show_indexed_array($array);
      echo "<h2>Esborrar elements d'un array indexat</h2>";
      $array = array(3, 6, 9, 1);
      array_pop($array);
      show_indexed_array($array);
      echo "<h2>Afegir elements a un array associatiu</h2>";
      $array = array('a'=>3, 'b'=>6, 'c'=>9, 'd'=>1);
      $array['e']=2;
      show_associative_array($array);
      echo "<h2>Eliminar elements d'un array associatiu</h2>";
      $array = array('a'=>3, 'b'=>6, 'c'=>9, 'd'=>1);
      unset($array['b']);
      show_associative_array($array);
      echo "<h2>Un array indexat és un array associatiu</h2>";
      $array = array(3, 6, 9, 1);
      $array[4]=2;
      unset($array[1]);
      show_associative_array($array);
      echo "<h2>Funcions per arrays</h2>";
      echo '<a href="https://www.php.net/manual/en/ref.array.php">Funcions per arrays</a>';
       ?>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>