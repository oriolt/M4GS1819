<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title> Exemple 14 (Funció) de procediments i funcions de php </title>
  </head>
  <body>
    <main>
        <p>Prendre un parell de valors i tractar-los en funció d’una operació (suma, resta, producte o divisió) mitjançant una funció.</p>
      <?php
  function operacions($n1, $n2, $operacio) {
  $resultat = 0;
   if($operacio == "Sumar") {
    $resultat = $n1 + $n2;
    }else if($operacio == "Restar") {
     $resultat = $n1 - $n2;
    }else if($operacio == "Multiplicar") {
    $resultat = $n1 * $n2;
    }
  return $resultat; // Retorno el resultat
   }
  // Crido la FUNCIÓ operacions
  $r = operacions(8, 9, "Sumar");
   echo $r . "<br>";
  // O bé, podem imprimir directament
   echo operacions(25, 8, "Restar");
 ?>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>