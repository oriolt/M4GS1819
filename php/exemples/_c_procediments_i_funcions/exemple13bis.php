<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title> Exemple de procediments i funcions de php </title>
  </head>
  <body>
    <main>
        p>Prendrem una cadena de caràcters i l'escriurem de nou intercalant guions (-). Per fer-ho, definirem un procediment.</p>
      <?php
    function escriu_separa($cadena){ 
   	for ($i=0;$i<strlen($cadena);$i++){ 
      	// La funció strlen ens retorna la longitud del string ‘cadena’.
      	echo $cadena[$i]; 
      	if ($i<strlen($cadena)-1) 
         	echo "-"; // No retorno cap mena de resultat (return)
   	} 
} 
escriu_separa ("hola"); 
echo "<p>"; 
escriu_separa ("Hi ha un missatge en els cereals!! Diu: Oooooooooo"); 
 ?>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
